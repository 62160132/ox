
import java.util.*;

public class OX {

    public static void main(String[] args) {
        System.out.println("Welcome to OX Game");
        char[][] sel = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        int turn = 0;
        String winner = "";
        boolean win = false;
        Scanner et = new Scanner(System.in);

        System.out.println(" 1 2 3");
        for (int i = 0; i < 3; i++) {
            System.out.print(i + 1);
            for (int j = 0; j < 3; j++) {
                System.out.print(sel[i][j] + " ");
            }
            System.out.println("");

        }
        while (!win) {

            turn++;
            if (turn % 2 == 0) {
                System.out.println("o turn");
            } else {
                System.out.println("x turn");
            }
            System.out.println("Please input Row Col: ");
            boolean checkplace = false;
            while (!checkplace) {
                int p1 = et.nextInt();
                int p2 = et.nextInt();
                if (sel[p1-1][p2-1] == '-') {
                    if (turn % 2 == 0) {
                        sel[p1 - 1][p2 - 1] = 'O';
                        checkplace = true;
                    } else {
                        sel[p1 - 1][p2 - 1] = 'X';
                        checkplace = true;
                    }
                } else {
                    System.out.println("You can't  not place there!!!");
                    System.out.println("Please input another position.");
                }}

                System.out.println(" 1 2 3");
                for (int i = 0; i < 3; i++) {
                    System.out.print(i + 1);
                    for (int j = 0; j < 3; j++) {
                        System.out.print(sel[i][j] + " ");
                    }
                    System.out.println("");

                }
                if ((sel[0][0] == 'X' && sel[0][1] == 'X' && sel[0][2] == 'X')
                        || (sel[1][0] == 'X' && sel[1][1] == 'X' && sel[1][2] == 'X')
                        || (sel[2][0] == 'X' && sel[2][1] == 'X' && sel[2][2] == 'X')
                        || (sel[0][0] == 'X' && sel[1][0] == 'X' && sel[2][0] == 'X')
                        || (sel[0][1] == 'X' && sel[1][1] == 'X' && sel[2][1] == 'X')
                        || (sel[0][2] == 'X' && sel[1][2] == 'X' && sel[2][2] == 'X')
                        || (sel[0][0] == 'X' && sel[1][1] == 'X' && sel[2][2] == 'X')
                        || (sel[0][2] == 'X' && sel[1][1] == 'X' && sel[2][0] == 'X')) {
                    win = true;
                    winner = "Player X Win....";
                } else if ((sel[0][0] == 'O' && sel[0][1] == 'O' && sel[0][2] == 'O')
                        || (sel[1][0] == 'O' && sel[1][1] == 'O' && sel[1][2] == 'O')
                        || (sel[2][0] == 'O' && sel[2][1] == 'O' && sel[2][2] == 'O')
                        || (sel[0][0] == 'O' && sel[1][0] == 'O' && sel[2][0] == 'O')
                        || (sel[0][1] == 'O' && sel[1][1] == 'O' && sel[2][1] == 'O')
                        || (sel[0][2] == 'O' && sel[1][2] == 'O' && sel[2][2] == 'O')
                        || (sel[0][0] == 'O' && sel[1][1] == 'O' && sel[2][2] == 'O')
                        || (sel[0][2] == 'O' && sel[1][1] == 'O' && sel[2][0] == 'O')) {
                    win = true;
                    winner = "Player O Win....";
                } else if (turn >= 9) {
                    winner = "Draw";
                    win = true;
                }
            }
            System.out.println(winner);
            System.out.println("Bye Bye ....");
        }
    }
